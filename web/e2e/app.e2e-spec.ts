import {AppPage} from './app.po';
import {browser, by, element} from 'protractor';
import {environment} from '../src/environments/environment';

describe('trip-organiser App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should register properly', () => {
    browser.get(environment.testServerUrl + '/#/auth/register');
    element(by.css('[id=input-name]')).sendKeys('Some user');
    element(by.css('[id=input-email]')).sendKeys('a@a.a');
    element(by.css('[id=input-password]')).sendKeys('aaaa');
    element(by.css('[id=input-re-password]')).sendKeys('aaaa');

    browser.waitForAngularEnabled(false);
    browser.switchTo().frame(0);
    const checkmark = element(by.css('[class=recaptcha-checkbox-checkmark]'));
    checkmark.click();
    browser.waitForAngularEnabled(true);

    browser.driver.sleep(1500);

    browser.switchTo().defaultContent();
    element(by.css('[class="customised-control-indicator"]')).click();
    element(by.css('[class="btn btn-block btn-hero-success"]')).click();
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toEqual(environment.testServerUrl + '/#/pages/dashboard');
  });

  it('should login properly', () => {
    browser.get(environment.testServerUrl + '/#/auth/login');
    element(by.css('[id=input-email]')).sendKeys('a@a.a');
    element(by.css('[id=input-password]')).sendKeys('aaaa');
    element(by.css('[class="btn btn-block btn-hero-success"]')).click();
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toEqual(environment.testServerUrl + '/#/pages/dashboard');
  });
})
;
