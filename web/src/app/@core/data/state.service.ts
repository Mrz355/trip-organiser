import {Injectable, NgZone} from '@angular/core';

import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/of';

@Injectable()
export class StateService {

  markers: Array<google.maps.Marker> = [];
  firstMarker: google.maps.Marker;
  lastMarker: google.maps.Marker;
  startEndPointSame: boolean;


  protected sidebars: any = [
    {
      name: 'Left Sidebar',
      icon: '',
      id: 'left',
      selected: true,
    },
    {
      name: 'Right Sidebar',
      icon: '',
      id: 'right',
    },
  ];

  protected sidebarState$ = new BehaviorSubject(this.sidebars[0]);

  setLayoutState(state: any): any {
  }

  constructor(private ngZone: NgZone) {
  }

  // addPlace(marker: any) {
  //   this.ngZone.run(() => this.layouts.push({
  //     name: marker.address,
  //     icon: 'nb-layout-default',
  //     id: 'one-column',
  //     selected: true,
  //   }));
  // }

  getLayoutStates(): Observable<any> {
    return Observable.of('');
  }

  setSidebarState(state: any): any {
    this.sidebarState$.next(state);
  }

  getSidebarStates(): Observable<any[]> {
    return Observable.of(this.sidebars);
  }

  onSidebarState(): Observable<any> {
    return this.sidebarState$.asObservable();
  }

  addMarker(marker: google.maps.Marker) {
    this.ngZone.run(() => this.markers.push(marker)
      // name: marker.address,
      // icon: 'nb-layout-default',
      // id: 'one-column',
      // selected: true,
    );
    if (this.firstMarker === undefined) {
      this.firstMarker = marker;
      return;
    }
    this.lastMarker = marker;
  }

  public deleteMarker(marker: google.maps.Marker) {
    this.removeMarkerFromMap(marker);
    this.markers.splice(this.markers.indexOf(marker), 1);
    if (this.firstMarker === marker) this.firstMarker = undefined;
    if (this.lastMarker === marker) this.lastMarker = undefined;
  }

  removeMarkerFromMap(marker: google.maps.Marker) {
    marker.setMap(null);
  }
}
