import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CoreModule} from './@core/core.module';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {ThemeModule} from './@theme/theme.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {APP_BASE_HREF} from '@angular/common';
import {NB_AUTH_TOKEN_CLASS, NbAuthJWTToken} from '@nebular/auth';
import {AuthGuardService} from './pages/auth-guard.service';
import {JWTNullSafeInterceptor} from './@theme/components/auth/jwt-null-safe-interceptor';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,

    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/'},
    {provide: NB_AUTH_TOKEN_CLASS, useValue: NbAuthJWTToken},
    {provide: HTTP_INTERCEPTORS, useClass: JWTNullSafeInterceptor, multi: true},
    AuthGuardService,
  ],
})
export class AppModule {
}
