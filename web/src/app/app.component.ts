import {Component, OnInit} from '@angular/core';
import {AnalyticsService} from './@core/utils/analytics.service';
import {ToasterConfig, ToasterService} from 'angular2-toaster';

@Component({
  selector: 'ngx-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

  title = 'Trip Organiser';
  //
  // config: ToasterConfig;

  constructor(private analytics: AnalyticsService) {
  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
    // this.config = new ToasterConfig({
    //   positionClass: 'toast-top-center',
    //   timeout: 2000,
    //   newestOnTop: true,
    //   tapToDismiss: true,
    //   preventDuplicates: false,
    //   animation: 'flyLeft',
    //   limit: 1,
    // });
  }
}
