import {Component, OnInit} from '@angular/core';
import {TripService} from './trip.service';
import {TripInfo} from './trip-info/trip-info';

@Component({
  selector: 'ngx-search-map',
  templateUrl: './search-map.component.html',
})
export class SearchMapComponent implements OnInit {
  constructor(private tripService: TripService) {
  }

  tripInfo: TripInfo;

  ngOnInit(): void {
    this.tripInfo = this.tripService.tripInfo;
  }
}
