import { Injectable } from '@angular/core';

@Injectable()
export class InfoWindowService {

  constructor() { }

  createInfoWindowWithPlaceMarkerButton(place, buttonHandler): HTMLElement {
    const icon = document.createElement('img');
    icon.src = place.icon;
    icon.setAttribute('style', 'height: 16px; width: 16px');

    const title = document.createElement('span');
    title.textContent = ' ' + place.name;
    title.setAttribute('style', 'font-weight: bold; font-size: 14px');

    const address = document.createElement('span');
    address.textContent = place.formatted_address;

    const placeButton = document.createElement('button');
    placeButton.className = 'map-info-button btn btn-hero-success';
    placeButton.setAttribute('style', 'height: 5px; width: fit-content; overflow: hidden; font-size: 10px; ' +
      'padding: 0 20px 22px 15px; text-align: center; margin: auto; display: block');
    placeButton.textContent = 'Place Marker';
    placeButton.addEventListener('click', () => {
      buttonHandler();
    });

    const div = document.createElement('div');
    div.appendChild(icon);
    div.appendChild(title);
    div.appendChild(document.createElement('br'));
    div.appendChild(address);
    div.appendChild(document.createElement('br'));
    div.appendChild(document.createElement('br'));
    div.appendChild(placeButton);

    return div;
  }

  createInfoWindowWithDeleteButton(place, buttonHandler): HTMLElement {
    let icon = undefined;
    if(place.icon) {
      icon = document.createElement('img');
      icon.src = place.icon;
      icon.id = 'icon';
      icon.setAttribute('style', 'height: 16px; width: 16px');
    }

    let title = undefined;
    if(place.name) {
      title = document.createElement('span');
      title.textContent = ' ' + place.name;
      title.id = 'title';
      title.setAttribute('style', 'font-weight: bold; font-size: 14px');
    }

    const addressSpan = document.createElement('span');
    addressSpan.textContent = place.formatted_address;
    addressSpan.id = 'address';

    const deleteButton = document.createElement('button');
    deleteButton.className = 'map-info-button btn btn-hero-success';
    deleteButton.setAttribute('style', 'height: 5px; width: 10px; overflow: hidden; font-size: 10px; ' +
      'padding: 0 50px 22px 15px; text-align: center; margin: auto; display: block');
    deleteButton.textContent = 'Delete';
    deleteButton.addEventListener('click', () => {
      buttonHandler();
    });

    const div = document.createElement('div');
    if(icon)  div.appendChild(icon);
    if(title) div.appendChild(title);
    div.appendChild(document.createElement('br'));
    div.appendChild(addressSpan);
    div.appendChild(document.createElement('br'));
    div.appendChild(document.createElement('br'));
    div.appendChild(deleteButton);

    return div;
  }

}
