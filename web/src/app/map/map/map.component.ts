import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {} from '@types/googlemaps'; // needed for google namespace to be visible to tslint
import {StateService} from '../../@core/data/state.service';
import {TripService} from '../trip.service';
import {InfoWindowService} from '../info-window.service';

declare const google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  @ViewChild('gmap') gmapElement;
  map: google.maps.Map;
  TravelMode = google.maps.TravelMode;

  currentLat: any;
  currentLong: any;
  uniqueId = 0;
  geocoder = new google.maps.Geocoder();
  placesService: google.maps.places.PlacesService;
  directionsService: google.maps.DirectionsService;
  directionsDisplay: google.maps.DirectionsRenderer;
  startEndPointSame: boolean = false;


  constructor(private stateService: StateService, private ngZone: NgZone, private tripService: TripService,
              private infoWindowService: InfoWindowService) {
  }

  ngOnInit() {
    this.initializeMap();
    this.findMe();
    const searchBox = new google.maps.places.SearchBox(document.getElementById('search-map'));

    google.maps.event.addListener(searchBox, 'places_changed', () => {
      const place = searchBox.getPlaces()[0];

      if (!place || !place.geometry) {
        // this.showToast();
        return;
      }

      if (place.geometry.viewport) {
        this.map.fitBounds(place.geometry.viewport);
      } else {
        this.map.setCenter(place.geometry.location);
        this.map.setZoom(16);
      }
      if (place.geometry.location) {
        this.addMarker(place);
      }

    });

    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});


    google.maps.event.addListener(this.map, 'bounds_changed', () => {
      const bounds = this.map.getBounds();
      searchBox.setBounds(bounds);
    });

    this.directionsDisplay.setMap(this.map);
  }

  setStartEndPoint(event) {
    this.startEndPointSame = !!event.target.checked;
    this.stateService.startEndPointSame = this.startEndPointSame;
  }

  calcRoute() {
    const markers = this.stateService.markers;
    if (markers.length < 2) return;
    const start = markers[0];
    let end = start;
    if (!this.startEndPointSame) {
      end = markers[markers.length - 1];
    }
    const waypts = markers
      .filter(marker => marker !== start && marker !== end)
      .map(marker => {
        return {
          location: marker.getPosition(),
          stopover: true
        };
      });
    const request = {
      origin: start.getPosition(),
      destination: end.getPosition(),
      travelMode: this.TravelMode.DRIVING,
      waypoints: waypts,
      optimizeWaypoints: true
    };

    this.directionsService.route(request, (result, status) => {
      if (status.toString() === 'OK') {
        this.directionsDisplay.setDirections(result);
        this.tripService.updateTripInfo(result.routes[0].legs);
        const swap = function (array, x, y) {
          const b = array[x];
          array[x] = array[y];
          array[y] = b;
        };
        const markersIndexes: Array<number> = markers.map((element, i) => i);
        for (let i = 1; i < markers.length - 1; ++i) {
          const newIndex = result.routes[0].waypoint_order[i - 1] + 1;
          const indexToSwap = markersIndexes.indexOf(newIndex);
          swap(markersIndexes, i, indexToSwap);
          swap(markers, i, indexToSwap);
        }
      } else {
        alert('Couldn\'t find route.\nMaybe you\'ve choosen too much points or the chosen travel mode is not available between these ' +
          'points.');
      }
    });
  }


  //
  // private showToast() {
  //   const toast: Toast = {
  //     type: 'warning',
  //     title: 'Error',
  //     body: 'Could not find typed place',
  //     timeout: 2000,
  //     showCloseButton: true,
  //     bodyOutputType: BodyOutputType.TrustedHtml,
  //   };
  //   this.toasterService.popAsync(toast);
  // }
  private initializeMap() {
    const mapProperties = {
      center: new google.maps.LatLng(52, 19),
      zoom: 6,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProperties);
    this.placesService = new google.maps.places.PlacesService(this.map);
    this.map.addListener('click', (event) => {
      if (!event.placeId) this.getLocation((place) => {
        this.addMarker(place);
      }, event);
      else {
        event.stop();
        this.getPlace((place) => this.addInfoWindow(place), event);

      }
    });
  }

  private getPlace(callback, event) {
    this.placesService.getDetails({'placeId': event.placeId}, (place, status) => {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        callback(place);
      }
    });
  }

  private addInfoWindow(place) {
    const infoWindow = new google.maps.InfoWindow({
      content: this.infoWindowService.createInfoWindowWithPlaceMarkerButton(place, () => {
        infoWindow.close();
        this.addMarker(place);
      })
    });

    infoWindow.setPosition(place.geometry.location);

    infoWindow.open(this.map);
  }

  private getLocation(callBack, event) {
    const location = event.latLng;

    this.geocoder.geocode({'location': location}, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK) {
        results[0].geometry.location = location;
        this.map.setCenter(results[0].geometry.location);
        callBack(results[0]);
      }
    });
  }

  private findMe() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.showPosition(position);
      });
    }
  }

  private showPosition(position) {
    this.currentLat = position.coords.latitude;
    this.currentLong = position.coords.longitude;

    const location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    this.map.panTo(location);
  }

  private addMarker(place) {
    const location = place.geometry.location;
    const address = place.formatted_address;
    const markerId = this.uniqueId++;
    const addedMarker = new google.maps.Marker({
      id: markerId,
      position: location,
      map: this.map,
      title: 'Your marker',
      draggable: true,
      animation: google.maps.Animation.DROP,
      address: address,
      clickedOnEvent: ((marker) => {
        this.map.setCenter(marker.position);
        this.stateService.markers.filter((m: any) => this.closeInfoWindow(m));
        this.openInfoWindow(marker);
      })
    });

    this.stateService.addMarker(addedMarker);

    const div = this.infoWindowService.createInfoWindowWithDeleteButton(place, () => this.stateService.deleteMarker(addedMarker));

    const infoWindow = new google.maps.InfoWindow({
      content: div
    });

    if (div.children['title']) addedMarker.name = div.children['title'].textContent;
    addedMarker.infoWindow = infoWindow;

    this.stateService.markers.filter((value: any) => this.closeInfoWindow(value));

    this.openInfoWindow(addedMarker);
    infoWindow.addListener('closeclick', () => this.closeInfoWindow(addedMarker));

    addedMarker.addListener('click', () => {
      this.stateService.markers.filter((value: any) => this.closeInfoWindow(value));
      this.openInfoWindow(addedMarker);
    });


    addedMarker.addListener('dragstart', (event) => {
      this.closeInfoWindow(addedMarker);
    });

    addedMarker.addListener('dragend', (event) => {
      this.getLocation((place) => {
        addedMarker.placeTmp = place;
        addedMarker.setPosition(event.latLng, place.formatted_address);
        addedMarker.address = place.formatted_address;
        this.stateService.markers.filter((value: any) => this.closeInfoWindow(value));
        this.openInfoWindow(addedMarker);
        div.children['address'].textContent = place.formatted_address;
        if (div.children['title']) div.removeChild(div.children['title']);
        if (div.children['icon']) div.removeChild(div.children['icon']);
      }, event);
    });
  }

  private closeInfoWindow(marker) {
    marker.infoWindow.close();
    this.ngZone.run(() => marker.selected = false);
  }

  private openInfoWindow(marker) {
    marker.infoWindow.open(marker.map, marker);
    this.ngZone.run(() => marker.selected = true);
  }
}
