import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapComponent } from './map.component';
import {NbCheckboxModule} from '@nebular/theme';
import {NgZone} from '@angular/core';
import {StateService} from '../../@core/data/state.service';
import {InfoWindowService} from '../info-window.service';
import {TripService} from '../trip.service';
import {} from '@types/googlemaps'; // needed for google namespace to be visible to tslint

declare var google: any;

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NbCheckboxModule],
      declarations: [ MapComponent ],
      providers: [
        StateService, TripService, InfoWindowService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
