export class TripInfo {
  totalDistance: string;
  totalDuration: string;
}
