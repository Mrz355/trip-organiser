import {Component, OnInit} from '@angular/core';
import {TripService} from '../trip.service';
import {TripInfo} from './trip-info';

@Component({
  selector: 'ngx-trip-info',
  templateUrl: './trip-info.component.html',
  styleUrls: ['./trip-info.component.scss']
})
export class TripInfoComponent implements OnInit {

  tripInfo: TripInfo;

  constructor(private tripService: TripService) {
  }

  ngOnInit() {
    this.tripInfo = this.tripService.tripInfo;
  }

}
