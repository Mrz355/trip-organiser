import {Injectable, NgZone} from '@angular/core';
import {TripInfo} from './trip-info/trip-info';

@Injectable()
export class TripService {
  tripInfo: TripInfo;

  constructor(private ngZone: NgZone) {
    this.tripInfo = new TripInfo();
    this.initDistanceUnits();
    this.initDurationUnits();
  }

  updateTripInfo(legs: google.maps.DirectionsLeg[]) {
    let totalDistance = 0;
    let totalDuration = 0;
    legs.forEach(leg => {
      totalDistance += leg.distance.value;
      totalDuration += leg.duration.value;
    });
    this.ngZone.run(() => {
      this.tripInfo.totalDistance = this.convert(totalDistance, this.distanceUnits);
      this.tripInfo.totalDuration = this.convert(totalDuration, this.durationUnits);
    });
  }

  private distanceUnits: Map<String, Number>;
  private durationUnits: Map<String, Number>;


  private initDistanceUnits() {
    this.durationUnits = new Map<String, Number>();
    this.durationUnits['h'] = 3600;
    this.durationUnits['m'] = 60;
    this.durationUnits['s'] = 1;
  }

  private initDurationUnits() {
    this.distanceUnits = new Map<String, Number>();
    this.distanceUnits['km'] = 1000;
    this.distanceUnits['m'] = 1;
  }

  private convert(seconds, units) {
    let value: number;
    let result = '';
    for (const [unit, val] of Object.entries(units)) {
      value = Math.floor(seconds / val);
      seconds -= value * val;
      result += value;
      result += unit;
      result += ' ';
    }
    return result;
  }
}
