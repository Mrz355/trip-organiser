import {TestBed, inject} from '@angular/core/testing';

import {AuthGuardService} from './auth-guard.service';
import {NbAuthService, NbAuthSimpleToken, nbCreateToken, NbTokenService, NbTokenStorage} from '@nebular/auth';
import {NbAuthToken} from '@nebular/auth/services/token/token';
import {Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';

describe('AuthGuardService', () => {
  beforeEach(() => {
    const tokenMock = {
      provide: NbTokenService,
      useValue: {
        get: new NbAuthSimpleToken('KURWA MOJ TOKEN')
      }
    };

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      providers: [AuthGuardService, NbAuthService, NbTokenStorage, {provide: NbTokenService, useValue: tokenMock }]
    });
  });

  it('should be created', inject([AuthGuardService], (service: AuthGuardService) => {
    expect(service).toBeTruthy();
  }));
});
