import {NgModule} from '@angular/core';


import {ThemeModule} from '../../@theme/theme.module';
import {DashboardComponent} from './dashboard.component';
import {MapComponent} from '../../map/map/map.component';
import {SearchMapComponent} from '../../map/search-map.component';
import {SearchComponent} from '../../map/search/search.component';
import {ToasterModule} from 'angular2-toaster';
import {TripInfoComponent} from '../../map/trip-info/trip-info.component';
import {TripService} from '../../map/trip.service';
import {InfoWindowService} from "../../map/info-window.service";


@NgModule({
  imports: [
    ThemeModule,
    ToasterModule
  ],
  declarations: [
    DashboardComponent,
    MapComponent,
    SearchMapComponent,
    SearchComponent,
    TripInfoComponent
  ],
  providers: [
    TripService,
    InfoWindowService
  ]
})
export class DashboardModule {
}
