import {Component, Input, OnInit, ViewChild} from '@angular/core';

import {NbMenuService, NbSidebarService} from '@nebular/theme';
import {UserService} from '../../../@core/data/users.service';
import {AnalyticsService} from '../../../@core/utils/analytics.service';
import {NbAuthJWTToken, NbAuthResult, NbAuthService} from '@nebular/auth';
import {Router} from '@angular/router';
import {NbUserComponent} from '@nebular/theme/components/user/user.component';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {


  @Input() position = 'normal';

  @ViewChild(NbUserComponent)
  userPane: NbUserComponent;

  user: any;

  loggedOut = false;

  userMenu = [{title: 'Profile'}, {title: 'Log out'}];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private userService: UserService,
              private analyticsService: AnalyticsService,
              private authService: NbAuthService,
              private router: Router) {
    this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {
        if (token.isValid()) {
          this.user = token.getPayload();
        }
      });
    this.menuService.onItemClick()
      .subscribe((event) => {
        this.onContecxtItemSelection(event.item.title);
      });
  }


  onContecxtItemSelection(title) {
    if (!this.loggedOut) {
      this.loggedOut = true;
      if (title === 'Log out') {
        // this.userPane.isMenuShown = false; // needed to log in logout and log in properly
        localStorage.removeItem('auth_app_token');
        this.authService.logout('email').subscribe((result: NbAuthResult) => {
          const redirect = result.getRedirect();
          if (redirect) {
            setTimeout(() => {
              return this.router.navigateByUrl(redirect);
            }, 100);
          }
        });
      } else {
        alert('Item not available at this moment.');
      }
    } else {
      this.loggedOut = false;
    }
  }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe((users: any) => this.user = users.nick);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }
}
