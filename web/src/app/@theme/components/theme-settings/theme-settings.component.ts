import {Component, NgZone, OnInit} from '@angular/core';

import {StateService} from '../../../@core/data/state.service';
import {DragulaService} from 'ng2-dragula';

@Component({
  selector: 'ngx-theme-settings',
  styleUrls: ['./theme-settings.component.scss'],
  template: `
    <h6 style="text-align: center;">PLACES</h6>
    <div>
      <div class='wrapper'>
        <div class='container' [dragula]='"third-bag"' [dragulaModel]='markers'>
          <div class='element' *ngFor="let marker of markers;
          let bagIndex = index" [attr.index]="bagIndex" [class.black]="this.stateService.startEndPointSame">
            <div
              (click)="layoutSelect(marker)">
              <nb-card>
                <nb-card-body [class.selected]="marker.selected">
                  <span *ngIf="marker.name"><b>{{marker.name}}</b><br></span>
                  {{marker.address}}
                  <!--[attr.title]="marker.name"-->
                </nb-card-body>
              </nb-card>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
})
export class ThemeSettingsComponent implements OnInit {

  markers: Array<google.maps.Marker>;
  sidebars = [];

  ngOnInit() {
    this.markers = this.stateService.markers;
  }

  constructor(protected stateService: StateService, private ngZone: NgZone, private dragulaService: DragulaService) {
    if (!this.dragulaService.find('third-bag')) {
      this.dragulaService.setOptions('third-bag', {removeOnSpill: true});
    }
    dragulaService.remove.subscribe((value) => {
      const [bagName, element] = value;
      this.stateService.removeMarkerFromMap(this.markers[element.getAttribute('index')]);
    });
    this.stateService.getSidebarStates()
      .subscribe((sidebars: any[]) => this.sidebars = sidebars);
  }

  layoutSelect(marker) {
    marker.clickedOnEvent(marker);
  }

  sidebarSelect(sidebars: any): boolean {
    this.sidebars = this.sidebars.map((s: any) => {
      s.selected = false;
      return s;
    });

    sidebars.selected = true;
    this.stateService.setSidebarState(sidebars);
    return false;
  }
}
