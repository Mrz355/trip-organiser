package pl.agh.tai.triporganiser.auth;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.http.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private RestTemplate restTemplate;

    public UserController(UserRepository userRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder,
                          RestTemplate restTemplate) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.restTemplate = restTemplate;
    }

    @PostMapping("/sign-up")
    public ResponseEntity signUp(@RequestBody ApplicationUser applicationUser) {
        String pass = applicationUser.getPassword();
        applicationUser.setPassword(bCryptPasswordEncoder.encode(applicationUser.getPassword()));
        userRepository.save(applicationUser);
        return login(applicationUser.getEmail(), pass);
    }

    private ResponseEntity login(String email, String pass) {
        RestTemplate restTemplate = new RestTemplate();

        String url = "http://localhost:8088/login";
        String requestJson = String.format("{\"email\":\"%s\", \"password\": \"%s\"}", email, pass);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);
        String response = restTemplate.postForObject(url, entity, String.class);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/sign-out")
    public ResponseEntity logout(HttpServletRequest request, HttpServletResponse response){
        //add this token to blacklist....
        System.out.println("LOGOUT succesfull");
        return ResponseEntity.ok().build();
    }
}
