package pl.agh.tai.triporganiser.auth;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<ApplicationUser, Long> {
    ApplicationUser findByEmail(String email);
    ApplicationUser findByUsername(String username);
}
