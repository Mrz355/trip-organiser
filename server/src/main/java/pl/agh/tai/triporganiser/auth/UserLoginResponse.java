package pl.agh.tai.triporganiser.auth;

import java.io.Serializable;

public class UserLoginResponse implements Serializable {
    private String token;

    public UserLoginResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
