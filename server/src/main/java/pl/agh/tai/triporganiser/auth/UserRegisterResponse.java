package pl.agh.tai.triporganiser.auth;

public class UserRegisterResponse {
    private String token = "embeddedToken";

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
